import { Component, OnInit, TemplateRef, ViewChild, ElementRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { isNumber } from 'util';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {

  rowForm = {
    category:"",
    make:"",
    price: null,
    date:""
  }

  @ViewChild("template") template: TemplateRef<any>;

  modalRef: BsModalRef;
  editMode = false
  gridApi: any;
  rowSelection: any;
  gridColumnApi: any;
  constructor(private modalService: BsModalService) {this.rowSelection="single"}
  errorMassage="";

  ngOnInit(){
    this.rowData=JSON.parse(localStorage.getItem('data')) || []

  }
  
    title = 'app';
    
    columnDefs = [
      {headerName: 'Name', field: 'make', sortable: true, filter: true},
      {headerName: 'Category', field: 'category', sortable: true, filter: true},
      {headerName: 'Price', field: 'price', sortable: true, filter: true},
      {headerName: 'Created Date', field: 'date', sortable: true, filter: true}
  ];
  
  
    rowData = [
      
    ];


    getRowData() {
      var rowData = [];
      this.gridApi.forEachNode(function(node) {
        rowData.push(node.data);
      });
    }

    onEditSelected(){
      let row = this.gridApi.getSelectedRows()[0]
      if(row)
        this.assignFormWithObj(row)
    }

    assignFormWithObj(obj){
      this.rowForm = obj
      this.editMode = true
      this.openModal(this.template)

    }

    addProduct(){
      this.rowData.push({})
      this.save()
    }

    save(){
      this.editMode = false;
      localStorage.setItem('data', JSON.stringify(this.rowData))
      if(this.modalRef){
        this.modalRef.hide()
      }
      this.gridApi.setRowData(this.rowData)
      
    }

    onRemoveSelected() {
      var selectedData = this.gridApi.getSelectedRows();
      var res=this.gridApi.updateRowData({ remove: selectedData });
      let index = this.rowData.findIndex((o)=>{return o.make == selectedData[0].make && o.category == selectedData[0].category})
      if(index > -1){
        this.rowData.splice(index,1)
        this.save()
      }
      this.gridApi
    }
    onGridReady(params) {
      this.gridApi = params.api;
      this.gridColumnApi = params.columnApi;
    }

    openModal(template: TemplateRef<any>) {
      this.modalRef = this.modalService.show(template);
  }

  saveForm(){

    console.log(this.rowForm)
  this.errorMassage="";

   if (this.rowForm.make.length>50 || !this.rowForm.make){
     this.errorMassage+="name should be less than  50 characters\n"
   }
   if(!this.rowForm.category){
     this.errorMassage+="Category is required \n"
   }
   if( !this.rowForm.price || !isNumber(this.rowForm.price) || this.rowForm.price < 0){
    this.errorMassage+="Price is required and should be greater than zero"
  }
  if (!this.errorMassage){
    if(!this.editMode)
        this.rowData.push({make:this.rowForm.make,category:this.rowForm.category,price:this.rowForm.price,date:this.rowForm.date});
    this.save();
  }
  
  
  }
    
}